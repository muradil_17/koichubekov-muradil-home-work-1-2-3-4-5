import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {AppText} from './UI/AppText';

export const Todo = ({todo, onRemove, onOpen})=>{
    return (
        <TouchableOpacity
            activeOpacity={0.5}
            onPress={() => onOpen(todo.id)}
            onLongPress={onRemove.bind(null, todo.id)}
            >
            <View style={styles.todo}>
                <AppText style={styles.text}>{todo.title}</AppText>
            </View>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
   todo: {
       flexDirection: "row",
       backgroundColor: "#fff",
       borderRadius: 5,
       padding: 5,
       marginTop: 8,
       borderWidth: 2,
       borderColor: "#456",
       width: "auto"
   },
   text: {
       color: "#000",
       fontSize: 18,
       textTransform: "capitalize",
       width: "85%",
   }
});