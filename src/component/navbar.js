import React from 'react';
import {View, Platform, StyleSheet} from 'react-native';
import {THEME} from "../theme";
import {HeaderText} from "./UI/HeaderText";

export const Navbar = ({title}) =>{
  return(
    <View style={{...styles.navBar, ...Platform.select({
            ios: styles.navBarIos,
            android: styles.navBarAndroid,
        })}}>
        <HeaderText style={styles.text}>{title}</HeaderText>
    </View>
  )
};

const styles = StyleSheet.create({
    navBar: {
        height: 90,
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingBottom: 20
    },
    navBarAndroid:{
        backgroundColor: THEME.DANGER_COLOR,
    },
    navBarIos:{
      backgroundColor: THEME.MAIN_COLOR,
    },
    text:{
        color: 'white',
        fontSize: 20
    }
});