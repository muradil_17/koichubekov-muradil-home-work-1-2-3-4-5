import React, {useState} from 'react';
import { StyleSheet,  View, TextInput, Button, Alert, Keyboard } from 'react-native';
import {MaterialIcons} from "@expo/vector-icons";


export  const AddTodo = ({onSubmit}) =>{
    const  [value, setValue] = useState("");
    const pressHandler = () =>{
        if (value.trim()){
            onSubmit(value);
            setValue("");
            Keyboard.dismiss()
        }else {
            Alert.alert("Error")
        }
    };

    return(
        <View style={styles.block}>
            <TextInput
                style={styles.input}
                onChangeText={setValue}
                value = {value}
                placeholder="Enter please text..."
            />
            <View style={styles.Button}>
                <MaterialIcons.Button style={styles.TextButton} onPress={pressHandler}  name="add-box">
                    Добавить
                </MaterialIcons.Button>
            </View>
        </View>
    )
};

const styles =StyleSheet.create({
   block: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
       paddingHorizontal: 30,
       paddingVertical: 30
   },
   input: {
     width: "65%",
     marginLeft: 10,
     borderBottomWidth: 3,
   },
   Button:{
       width: "35%",
   },
   TextButton:{
       color: "#fff"
   }
});