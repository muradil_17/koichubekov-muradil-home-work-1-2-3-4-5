import React from 'react';
import {View, StyleSheet} from 'react-native';

export const AppCard = props =>{
    return(
        <View style={styles.default}>
            {props.children}
        </View>
    )
};

const styles = StyleSheet.create({
   default: {
       backgroundColor: "#fff",
       borderRadius: 5,
       shadowRadius: 2,
       shadowOpacity: 0.4,
       shadowOffset: {width: 2, height: 2},
       elevation: 8,
       padding: 15,
       flexDirection: "row",
       justifyContent: "space-between",
       alignItems: "center"
   }
});