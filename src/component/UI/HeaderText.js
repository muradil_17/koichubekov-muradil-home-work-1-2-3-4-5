import React from 'react';
import {StyleSheet, Text} from 'react-native';

export const HeaderText = props => (
    <Text style={{...style.default, ...props.style}}>{props.children}</Text>
);

const style = StyleSheet.create({
    default: {
        fontFamily: "Lora-Regular"
    }
});