import React from 'react';
import {StyleSheet, View, TouchableOpacity, Platform, TouchableNativeFeedback} from 'react-native';
import {AppText} from "./AppText";
import {THEME} from "../../theme";

export const AppButton = ({children, onPress, color = THEME.BACK_COLOR }) => {

    const TouchableWrapper = Platform.OS === 'android' ? TouchableNativeFeedback: TouchableOpacity;


    return(
        <TouchableWrapper onPress={onPress} activeOpacity={0.7}>
            <View style={{...style.button, backgroundColor: color}} >
                <AppText>
                    {children}
                </AppText>
            </View>
        </TouchableWrapper>
    )
};

const style = StyleSheet.create({
   button: {
       paddingHorizontal: 5,
       paddingVertical: 4,
       borderRadius: 7,
       flexDirection: "row",
       alignItems: "center",
       justifyContent: "center",
       color: "#fff"
   }
});