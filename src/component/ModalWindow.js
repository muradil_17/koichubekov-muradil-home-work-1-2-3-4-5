import React, {useState} from 'react';
import {Modal, View, TextInput, Button, StyleSheet, Alert} from 'react-native';
import {THEME} from "../theme";

export const ModalWindow = ({visible, onCancel, value, onSave}) =>{
  const [text, setText] = useState(value);
  const onSaver = () =>{
      if (text.trim()){
          onSave(text)
      } else {
          Alert.alert(
              "Error!",
              "Название заметки не может быть пустым"
          )
      }

  };
  const TextButton = {
    save: "Сохранить",
    cancel: "Отменить"
  };
  return(
      <Modal visible={visible} animationType="slide">
          <View style={style.wrapper}>
              <TextInput
                  style={style.input}
                  placeholder="Введите изменение"
                  value={text}
                  onChangeText={setText}
              />
              <View style={style.buttons}>
                <Button
                    title={TextButton.save}
                    color={THEME.BACK_COLOR}
                    onPress={onSaver}
                >
                </Button>
                <Button
                    title={TextButton.cancel}
                    color={THEME.DANGER_COLOR}
                    onPress={onCancel}
                >
                </Button>
              </View>
          </View>
      </Modal>
  )
};

const style = StyleSheet.create({
   wrapper: {
       flex: 1,
       alignItems: "center",
       justifyContent: "center"
   },
   input:{
       borderBottomWidth: 1,
       borderBottomColor: THEME.MAIN_COLOR,
       width: "80%"
   },
   buttons:{
       paddingTop: 10,
       flexDirection: "row",
       width: "100%",
       justifyContent: "space-around"
   }
});