import React, {useState, useContext} from 'react';
import {View, StyleSheet, Alert} from 'react-native';
import {Navbar} from "./component/navbar";
import {THEME} from "./theme";
import {MainScreen} from "./screen/MainScreen";
import {TodoScreen} from "./screen/TodoScreen";
import {TodoContext} from "./context/Todo/TodoContext";

export const MainLayout = () =>{
    const {todos, addTodo, removeTodo, updateTodo} = useContext(TodoContext);

    const [todoId, setTodoId] = useState(null);

    // const [todos, setTodos] = useState([]);


    // const updateTodo = (id, title) => {
    //     setTodos(prev=>
    //         prev.map(todo =>{
    //             if (todo.id === id ){
    //                 todo.title = title
    //             }
    //             return todo
    //         })
    //     )
    // };

    // const removeTodo = id => {
    //     const todo = todos.find(tod => tod.id === id);
    //
    //     Alert.alert(
    //         'Удаление заметки',
    //         `Подтвердите удаление заметки: ${todo.title} ?` ,
    //         [
    //             {
    //                 text: "Отменить",
    //                 style :'neutral',
    //             },
    //             {
    //                 text: 'Подтвердить',
    //                 style: 'negative',
    //                 onPress: () => {
    //                     setTodoId(null);
    //                     setTodos(prev => prev.filter(todo => todo.id !== id))
    //                 }
    //             },
    //         ],
    //         { cancelable: false }
    //     );
    // };

    // const addTodo = (title)=>{
    //     setTodos(prev => [
    //         ...prev,
    //         {
    //             id: Date.now().toString(),
    //             title
    //         }
    //     ])
    // };

    let content = (
        <MainScreen addTodo={addTodo} todos={todos} removeTodo={removeTodo} openTodo={ setTodoId}/>
    );

    if (todoId){
        const selectedTodo = todos.find(todo => todo.id === todoId);
        content = <TodoScreen todo={selectedTodo} goBack={() => setTodoId(null)} removeId={removeTodo} onSave={updateTodo}/>
    }

    return(
        <View>
            <Navbar title="Navbar"/>
            <View styles={styles.container}>
                {content}
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        height: "90%",
        paddingHorizontal: THEME.PADDING_HORIZONTAL,
        paddingVertical: THEME.PADDING_VERTICAL,
        justifyContent: "flex-end",
    }
});