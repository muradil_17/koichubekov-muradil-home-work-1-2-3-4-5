import React, {useState, useEffect} from 'react';
import {StyleSheet, View, Text, Button, Dimensions} from 'react-native';
import {THEME} from "../theme";
import {AppCard} from "../component/UI/AppCard";
import {AppText} from "../component/UI/AppText";
import {ModalWindow} from "../component/ModalWindow";
import {AppButton} from "../component/UI/AppButton";
import {FontAwesome} from  '@expo/vector-icons';
import {Feather} from  '@expo/vector-icons';
import {AntDesign} from  '@expo/vector-icons';

export const TodoScreen = ({todo, goBack, removeId, onSave}) =>{
    const textButton = {
      back: "назад",
      remove: "Удалить",
      create: "Редактировать"
    };

    const [modal, setModal] = useState(false);

    const onSaver = title =>{
      onSave(todo.id, title);
        setModal(false)
    };

    const [deviceWidth, setDeviceWdth] = useState(
        Dimensions.get('window').width / 3,
    );

    useEffect(()=>{
       const update = () =>{
           const width = Dimensions.get('window').width / 3;
           setDeviceWdth(width)
       };
       Dimensions.addEventListener("change", update);
       return () =>{
           Dimensions.removeEventListener("change", update);
       }
    });

    return(
        <View>
            <ModalWindow
                visible={modal}
                onCancel={() => setModal(false)}
                value={todo.title}
                onSave={onSaver}
            />
            <View style={{...style.buttonContainer, deviceWidth}}>
                <View style={style.buttonGoBack}>
                    <AppButton onPress={goBack} color={THEME.BACK_COLOR} >
                        <AntDesign name="arrowleft" size={35} />
                    </AppButton>
                </View>
                <View style={{...style.buttonRemove, deviceWidth}}>
                    <AppButton  onPress={ () => removeId(todo.id)} color={THEME.DANGER_COLOR} >
                        <FontAwesome name="remove" size={35} />
                    </AppButton>
                </View>
            </View>
            <View style={style.container}>
                <AppCard>
                    <AppText style={style.title}>{todo.title}</AppText>
                    <AppButton  onPress={() => setModal(true)} color={THEME.CREATE_COLOR} >
                        <Feather name="edit" size={35} />
                    </AppButton>
                </AppCard>
            </View>
        </View>
    )
};

const style = StyleSheet.create({
    container: {
        marginTop: 25,
        marginLeft: 15,
        marginRight: 15,
    },
    buttonContainer:{
        flexDirection: "row",
        justifyContent: "space-between"
    },
    buttonGoBack:{
        width: Dimensions.get("window").width / 3,
        marginTop: 15
    },
    buttonRemove:{
        width: Dimensions.get("window").width / 3,
        marginTop: 15
    },
    title:{
        width: "50%",
        fontSize: 30,
        textTransform: "capitalize",
    }
});