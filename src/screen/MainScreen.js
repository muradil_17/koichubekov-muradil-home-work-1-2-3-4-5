import React, {useState, useEffect} from 'react';
import {StyleSheet, View, FlatList, Image, Dimensions} from 'react-native';
import {Todo} from "../component/Todo";
import {AddTodo} from "../component/addtodo";
import {THEME} from "../theme";

export const MainScreen = ({addTodo, todos, removeTodo, openTodo}) =>{

    const [deviceWidth, setDeviceWidth] = useState(
        Dimensions.get("window").width - THEME.PADDING_HORIZONTAL * 20
    );

    useEffect(()=>{
       const update = () =>{
           const width = Dimensions.get("window").width - THEME.PADDING_HORIZONTAL * 20;
           setDeviceWidth(width)
       };
       Dimensions.addEventListener("change", update);
       return () =>{
           Dimensions.removeEventListener("change", update)
       }
    });


    let content = (
        <View style={{deviceWidth}}>
            <FlatList
            keyExtractor = {item => item.id}
            data={todos.reverse()}
            renderItem={({item}) => (<Todo  todo={item} onRemove={removeTodo} onOpen={openTodo}/>) }
        />
        </View>
    );

    if(!todos.length){
        content = (
            <View>
                <Image
                    style={style.image}
                    source={require('../../assets/no-content.png')}
                />
            </View>
        )
    }


  return(
      <View>
          <AddTodo  onSubmit={addTodo}/>
          <View style={style.imageWrapper}>{content}</View>
      </View>
  )
};

const style = StyleSheet.create({
    imageWrapper: {
      alignItems: "center",
      justifyContent: "center",
      padding: 10,
      height: 480
    },
    image:{
      resizeMode: "contain"
    },
});