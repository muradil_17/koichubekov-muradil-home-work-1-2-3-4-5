import React, {useState} from 'react';
import { Alert} from 'react-native';
import * as Font from 'expo-font';
import {AppLoading} from "expo";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import {Navbar} from "./src/component/navbar";
import {MainScreen} from "./src/screen/MainScreen";
import {TodoScreen} from "./src/screen/TodoScreen";
import {THEME} from "./src/theme";
import {MainLayout} from "./src/MainLayout";
import {TodoState} from "./src/context/Todo/TodoState";

async function FontConfiguration() {
   await Font.loadAsync({
       'Amiri-Regular': require('./Fonst/Amiri-Regular.ttf'),
       'Amiri-Italic': require('./Fonst/Amiri-Italic.ttf'),
       'Lora-Regular': require('./Fonst/HeaderFont/static/Lora-Regular.ttf')
   })
}

export default function App() {

  const [isReady, setIsReady] =useState(false);

  if (!isReady){
      return (
          <AppLoading
            startAsync = {FontConfiguration}
            onFinish = {() => setIsReady(true)}
            onError={error => console.log(error)}
          />
      )
  }

  return (
    <TodoState>
        <MainLayout/>
    </TodoState>
  );
}

